import pprint
from math import pi, acos, sqrt, cos, sin, asin

from imports.tex_templates import *


def ROUND(n):
    return round(round(n, 6), 2)


def sol_lll(sides, err, err_app, tex):
    l1 = sides["A"]["side"]
    l2 = sides["B"]["side"]
    l3 = sides["C"]["side"]

    a1 = get_arc_cos(l1, l2, l3, err, err_app)
    if len(err) == 0:
        tex.append(get_tikz_figure_precomplete(sides))
        sides["A"]["angle"] = a1
        get_side_lll(tex, l1, l2, l3, a1, "a", "b", "c")
        tex.append(r"""\scalebox{1.4}{$\begin{aligned}
        & a = %g \\
        & b = %g \\
        & c = %g \\
        & \alpha = %g^\circ \\
        \end{aligned}$}\\\\\\"""%(l1,l2,l3,ROUND(a1)))
        a2 = get_arc_cos(l2, l1, l3, err, err_app)
        if len(err) == 0:
            tex.append(get_tikz_figure_precomplete(sides))
            sides["B"]["angle"] = a2
            get_side_lll(tex, l2, l1, l3, a2, "b", "a", "c")
            tex.append(r"\scalebox{1.4}{$\beta = %g^\circ$} \\\\\\"%a2)
    a3 = ROUND(180 - a1 - a2)
    tex.append(get_tikz_figure_precomplete(sides))
    get_side_3(tex, a1, a2, a3)
    tex.append(r"\scalebox{1.4}{$\gamma = %g^\circ$}"%a3)

    sides["A"]["angle"] = a1
    sides["B"]["angle"] = a2
    sides["C"]["angle"] = a3

    only_sides = [
        [ROUND(l1), ROUND(a1), "A"],
        [ROUND(l2), ROUND(a2), "B"],
        [ROUND(l3), ROUND(a3), "C"],
    ]

    # sort_orders = sorted(only_sides, key=lambda x: x[0], reverse=True)
    
    pprint.pprint(only_sides)
    return only_sides


def sol_lal(s1, s2, a, sides, err, err_app, tex):
    l1 = sides[s1]["side"]
    l2 = sides[s2]["side"]
    a3 = sides[a]["angle"] * pi / 180
    l3 = get_side_from_2_sides_and_angle(l1, l2, a3)

    tex.append(get_tikz_figure_precomplete(sides))
    sides[a]["side"] = ROUND(l3)
    get_side_1_lla(tex, s1, s2, a, l1, l2, ROUND(a3*180/pi), l3)
    tex.append(r"""\scalebox{1.4}{$\begin{aligned}
            & %s = %g \\
            & %s = %g \\
            & %s = %g^\circ \\
            & %s = %g \\
            \end{aligned}$}\\\\\\""" % (
                s1.lower(), l1,
                s2.lower(), l2,
                get_symbol(a), ROUND(a3*180/pi),
                a.lower(), l3,
            )
    )
    # a1 = get_arc_cos(l1,l2,l3,err,err_app)
    a1 = get_arcsin(a3,l3,l1)
    a3 = ROUND(a3*180/pi)

    tex.append(get_tikz_figure_precomplete(sides))
    sides[s1]["angle"] = ROUND(a1)
    get_angle_lla(tex, a, s1, l3, l1, a3, a1)
    tex.append(r"\scalebox{1.4}{$ %s = %g^\circ$} \\\\\\"%(get_symbol(s1),a1))
    a2 = ROUND(180 - a3 - a1)

    tex.append(get_tikz_figure_precomplete(sides))
    get_last_angle_lla(tex, a, s2, s1, a3, a2, a1)
    # tex.append(r"\scalebox{1.4}{FINALES}")
    tex.append(r"\scalebox{1.4}{$ %s = %g^\circ$}" % (get_symbol(s2), a2))

    sides[a]["side"] = ROUND(l3)
    sides[s2]["angle"] = ROUND(a2)

    result = [
        [ROUND(sides[i]["side"]), ROUND(sides[i]["angle"]), i]
        for i in "ABC"
    ]
    pprint.pprint(result)
    return result


def sol_lla(os, swa, sides, err, err_app, tex):
    a3 = sides[swa]["angle"] * pi / 180

    c = ROUND(sides[os]["side"]) # El que solo tiene un lado
    b = ROUND(sides[swa]["side"]) # El que tiena lado y
    
    D = ROUND((c/b) * sin(a3))

    print(f"D = {D}")

    results = []

    if D > 1:
        err.append("Can't create a triangle with that data")
        err_app = err_app(err)
        err_app.run()
        return None
    if D <= 1:
        if b < c:
            print("b < c")
            TEX1 = []
            TEX2 = []
            y1 = ROUND(asin(D))
            #TEX1.append(r"Case 1: With $%s$ acute angle\\" % get_symbol(os))
            TEX1.append(get_tikz_figure_precomplete(sides))
            get_angle_from_sin_lla(TEX1, os, swa, c, b, a3, y1)
            TEX1.append(r"""\scalebox{1.4}{$\begin{aligned}
                        & %s = %g \\
                        & %s = %g \\
                        & %s = %g^\circ \\
                        & %s = %g^\circ \\
                        \end{aligned}$}\\\\\\""" % (
                os.lower(), c,
                swa.lower(), b,
                get_symbol(swa), ROUND(a3 * 180 / pi),
                get_symbol(os), ROUND(y1 * 180 / pi),
            )
            )
            # y = sides[c_]["angle"] = y1
            # Y1 step
            #print(f"{os}------------------------------------------")
            sides[os]["angle"] = ROUND(y1 * 180 / pi)
            last_angle = ROUND(pi - a3 - y1)
            faltante = list({"A", "B", "C"} - {os, swa})[0]
            TEX1.append(get_tikz_figure_precomplete(sides))
            sides[faltante]["angle"] = last_angle * 180 / pi
            get_last_angle_aal(TEX1, faltante, os, swa, ROUND(last_angle*180/pi), ROUND(y1*180/pi), ROUND(a3*180/pi))
            TEX1.append(r"\scalebox{1.4}{$ %s = %g^\circ$} \\\\\\" % (get_symbol(faltante), ROUND(last_angle*180/pi)))
            a = ROUND(b * sin(last_angle) / sin(a3))

            TEX1.append(get_tikz_figure_precomplete(sides))
            get_side_from_ala(TEX1, faltante, swa, a, ROUND(last_angle*180/pi), b, ROUND(a3*180/pi))
            TEX1.append(r"\scalebox{1.4}{$ %s = %g$}" % (faltante.lower(), a))
            #sides[swa]["angle"] = a3
            sides[faltante]["side"] = a

            result = [
                [ROUND(sides[i]["side"]), ROUND(sides[i]["angle"]), i]
                for i in "ABC"
            ]
            #sides[faltante]["angle"] = False
            results.append(result)
            # Y2 step
            # --------------------------------------------------
            # --------------------------------------------------
            # --------------------------------------------------
            #TEX2.append(r"Case 2: With $%s$ obtuse angle\\"%get_symbol(os))
            y2 = pi - y1
            TEX2.append(r" & ")
            sides[os]["angle"] = y2
            show_optusangle(TEX2, os, ROUND(y1*180/pi), ROUND(y2*180/pi))
            TEX2.append(r"""\scalebox{1.4}{$\begin{aligned}
                                    & %s = %g \\
                                    & %s = %g \\
                                    & %s = %g^\circ \\
                                    & %s' = %g^\circ \\
                                    \end{aligned}$}\\\\\\""" % (
                os.lower(), c,
                swa.lower(), b,
                get_symbol(swa), ROUND(a3 * 180 / pi),
                get_symbol(os), ROUND(y2 * 180 / pi),
            )
            )
            faltante = list({"A", "B", "C"} - {os, swa})[0]
            last_angle = pi - a3 - y2
            sides[os]["angle"] = ROUND(y2 * 180 / pi)
            sides[faltante]["angle"] = False
            TEX2.append(get_tikz_figure_precomplete(sides))
            sides[faltante]["angle"] = ROUND(last_angle * 180 / pi)
            get_last_angle_aal_prime(TEX2, faltante, os, swa, ROUND(last_angle*180/pi), ROUND(y2*180/pi), ROUND(a3*180/pi))
            TEX2.append(r"\scalebox{1.4}{$ %s = %g^\circ$} \\\\\\" % (get_symbol(faltante), ROUND(last_angle*180/pi)))
            a = ROUND(b * sin(last_angle) / sin(a3))
            TEX2.append(get_tikz_figure_precomplete(sides))
            get_side_from_ala(TEX2, faltante, swa, a, ROUND(last_angle*180/pi), b, ROUND(a3*180/pi))
            TEX2.append(r"\scalebox{1.4}{$ %s = %g$}" % (faltante.lower(), a))
            sides[swa]["angle"] = a3 * 180 / pi
            sides[faltante]["side"] = a

            result = [
                [ROUND(sides[i]["side"]), ROUND(sides[i]["angle"]), i]
                for i in "ABC"
            ]
            results.append(result)
            tex.append(TEX1)
            tex.append(TEX2)
        else:
            print("b >= c")
            # tex.append(r"$$D < 1 \,\,\, b \geq c$$")
            # tex.append(rf"$$b = {b}\,\,\,\, c = {c}$$")
            y = ROUND(asin(D))
            # print(y*180/pi)
            # print(a3*180/pi)
            tex.append(get_tikz_figure_precomplete(sides))
            get_angle_from_sin_lla(tex, os, swa, c, b, a3, y)
            tex.append(r"""\scalebox{1.4}{$\begin{aligned}
                        & %s = %g \\
                        & %s = %g \\
                        & %s = %g^\circ \\
                        & %s = %g^\circ \\
                        \end{aligned}$}\\\\\\""" % (
                os.lower(), c,
                swa.lower(), b,
                get_symbol(swa), ROUND(a3 * 180 / pi),
                get_symbol(os), ROUND(y * 180 / pi),
            )
            )
            last_angle = ROUND(pi - a3 - y)
            faltante = list({"A", "B", "C"} - {os, swa})[0]
            sides[os]["angle"] = ROUND( y * 180 / pi )
            tex.append(get_tikz_figure_precomplete(sides))
            get_last_angle_aal(tex, faltante, os, swa, ROUND(last_angle*180/pi), ROUND(y*180/pi), ROUND(a3*180/pi))
            #tex.append(r"\scalebox{1.4}{FINALES} \\\\\\")
            tex.append(r"\scalebox{1.4}{$ %s = %g^\circ$} \\\\\\" % (get_symbol(faltante), ROUND(last_angle*180/pi)))
            a = ROUND(b * sin(last_angle) / sin(a3))

            # get_angle_lla(tex, swa, faltante, a, b, a3*180/pi, last_angle*180/pi)
            sides[faltante]["angle"] = ROUND(last_angle * 180 / pi)
            tex.append(get_tikz_figure_precomplete(sides))
            get_side_from_ala(tex, faltante, swa, a, ROUND(last_angle*180/pi), b, ROUND(a3*180/pi))
            # tex.append(r"\scalebox{1.4}{FINALES}")
            tex.append(r"\scalebox{1.4}{$ %s = %g$}" % (faltante.lower(), a))
            #sides[swa]["angle"] = a3

            sides[faltante]["side"] = a

            results = [
                [ROUND(sides[i]["side"]), ROUND(sides[i]["angle"]), i]
                for i in "ABC"
            ]
    pprint.pprint(results)
    return results


def sol_ala(a1, a2, s, sides, tex):
    ang1 = sides[a1]["angle"] * pi / 180
    ang2 = sides[a2]["angle"] * pi / 180
    side3 = sides[s]["side"]

    ang3 = pi - ang1 - ang2
    tex.append(get_tikz_figure())
    get_last_angle_ala(tex, s, a2, a1, ROUND(ang3*180/pi), ROUND(ang2*180/pi), ROUND(ang1*180/pi))
    tex.append(r"""\scalebox{1.4}{$\begin{aligned}
                            & %s = %g \\
                            & %s = %g^\circ \\
                            & %s = %g^\circ \\
                            & %s = %g^\circ \\
                            \end{aligned}$}\\\\\\""" % (
        s.lower(), side3,
        get_symbol(a1), ROUND(ang1 * 180 / pi),
        get_symbol(a2), ROUND(ang2 * 180 / pi),
        get_symbol(s), ROUND(ang3 * 180 / pi),
        )
    )
    side1 = ROUND(side3 * sin(ang1) / sin(ang3))
    side2 = ROUND(side3 * sin(ang2) / sin(ang3))

    tex.append(get_tikz_figure())
    get_side_from_ala(tex, a1, s, side1, ROUND(ang1*180/pi), side3, ROUND(ang3*180/pi))
    tex.append(r"\scalebox{1.4}{$ %s = %g$} \\\\\\" % (a1.lower(), side1))

    tex.append(get_tikz_figure())
    get_side_from_ala(tex, a2, s, side2, ang2*180/pi, side3, ang3*180/pi)
    tex.append(r"\scalebox{1.4}{$ %s = %g$}" % (a2.lower(), side2))

    sides[a1]["side"] = side1
    sides[a2]["side"] = side2
    sides[a1]["angle"] = ang1
    sides[a2]["angle"] = ang2
    sides[s]["angle"] = ang3

    results = [
            [ROUND(sides[i]["side"]), ROUND((sides[i]["angle"] * 180/pi) % 360), i]
            for i in "ABC"
        ]
    pprint.pprint(results)
    return results


def sol_aal(a1, a2, s, sides, tex):
    faltante = list({"A", "B", "C"} - {a1, a2})[0]
    print([a1,a2])
    print(faltante)

    ang1 = sides[s]["angle"] * pi / 180
    ang2 = sides[a1]["angle"] * pi / 180
    side1 = sides[s]["side"]

    ang3 = pi - ang1 - ang2
    s_order = [a1,s]
    s_order.sort()
    tex.append(get_tikz_figure_precomplete(sides))
    get_last_angle_aal(tex, faltante, a2, a1, ROUND(ang3*180/pi), ROUND(ang1*180/pi), ROUND(ang2*180/pi))
    tex.append(r"""\scalebox{1.4}{$\begin{aligned}
                                & %s = %g \\
                                & %s = %g^\circ \\
                                & %s = %g^\circ \\
                                & %s = %g^\circ \\
                                \end{aligned}$}\\\\\\""" % (
        s.lower(), side1,
        get_symbol(s_order[0]), sides[s_order[0]]["angle"],
        get_symbol(s_order[1]), sides[s_order[1]]["angle"],
        get_symbol(faltante),ROUND(ang3 * 180 / pi),
    )
    )

    # print(ang3*180 / pi)
    # print(ang2*180 / pi)
    # print(ang1*180 / pi)
    sides[faltante]["angle"] = ang3 * 180 / pi

    side2 = ROUND(side1 * sin(ang2) / sin(ang1))
    side3 = ROUND(side1 * sin(ang3) / sin(ang1))

    #                     side y angle fijo, side y angle incognitos
    tex.append(get_tikz_figure_precomplete(sides))
    get_side_from_aal(tex, s, ROUND(ang1*180/pi), side1, a1, ROUND(ang2*180/pi), side2)
    tex.append(r"\scalebox{1.4}{$ %s = %g$} \\\\\\" % (a1.lower(), side2))
    sides[a1]["side"] = side2
    tex.append(get_tikz_figure_precomplete(sides))
    get_side_from_aal(tex, s, ROUND(ang1*180/pi), side1, faltante, ROUND(ang3*180/pi), side3)
    tex.append(r"\scalebox{1.4}{$ %s = %g$}" % (faltante.lower(), side3))
    # get_side_from_ala(tex, faltante, faltante, side3, side1, ang1*180/pi, ang3*180/pi,)

    sides[s]["angle"] = side1  * 180 / pi
    sides[s]["angle"] = ang1  * 180 / pi
    sides[a1]["angle"] = ang2  * 180 / pi


    sides[faltante]["side"] = side3

    results = [
            [ROUND(sides[i]["side"]), ROUND((sides[i]["angle"])), i]
            for i in "ABC"
        ]
    pprint.pprint(results)
    return results


def get_arc_cos(a, b, c, err, err_app):
    try:
        return ROUND(acos((b**2+c**2-a**2)/(2*b*c))*180/pi)
    except ValueError:
        err.append("The sum of two sides must be larger than the third.")
        err_app = err_app(err)
        err_app.run()


def get_side_from_2_sides_and_angle(s1, s2, angle):
    return ROUND(sqrt(s1**2 + s2**2 - (2*s1*s2*cos(angle))))


def get_arcsin(gamma, c, a):
    return ROUND(asin(sin(gamma)*a/c) * 180 / pi)

#def get_side_3_phantom(v):
#    if v == "A":
#        return r"\p"