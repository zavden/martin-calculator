import platform
from subprocess import call
import pprint
from imports.tex_templates import get_TEX, get_TEX2
from math import pi

def run_latex(ERRORS, RESULTS, TEX, SIDES):
    if len(ERRORS) == 0:
        current_os = platform.system()
        if len(RESULTS) != 2:
            A, B, C = RESULTS
            sort_vals = sorted(RESULTS, key=lambda x: x[0], reverse=True)
            MAX = sort_vals[0][2]
            if MAX == "A":
                a = A[0]
                b = B[0]
                gamma = C[1]
                progression = ["A","B","C"]
                colors = ["red", "blue", "green"]
            elif MAX == "B":
                a = B[0]
                b = C[0]
                gamma = A[1]
                progression = ["B", "C", "A"]
                colors = ["blue", "green", "red"]
            elif MAX == "C":
                a = C[0]
                b = A[0]
                gamma = B[1]
                progression = ["C", "A", "B"]
                colors = ["green", "red", "blue"]
            print(f"a = {a}\t b = {b}\t gamma = {gamma}")
            pprint.pprint(RESULTS)
            pprint.pprint(MAX)
            colors = ["black"]*3
            TEX = get_TEX(a, b, gamma, colors, RESULTS, TEX, progression, SIDES)
        else:
            A1, B1, C1 = RESULTS[0]
            A2, B2, C2 = RESULTS[1]
            sort_vals1 = sorted(RESULTS[0], key=lambda x: x[0], reverse=True)
            sort_vals2 = sorted(RESULTS[1], key=lambda x: x[0], reverse=True)
            progressions=[]
            sides1 = {
                "A": {
                    "side": A1[0],
                    "angle": A1[1]
                },
                "B": {
                    "side": B1[0],
                    "angle": B1[1]
                },
                "C": {
                    "side": C1[0],
                    "angle": C1[1]
                },
            }
            sides2 = {
                "A": {
                    "side": A2[0],
                    "angle": A2[1]
                },
                "B": {
                    "side": B2[0],
                    "angle": B2[1]
                },
                "C": {
                    "side": C2[0],
                    "angle": C2[1]
                },
            }
            sides = [sides1, sides2]
            MAX1 = sort_vals1[0][2]
            MAX2 = sort_vals2[0][2]
            if MAX1 == "A":
                a1 = A1[0]
                b1 = B1[0]
                gamma1 = C1[1]
                colors1 = ["red", "blue", "green"]
                progression1 = ["A", "B", "C"]
            elif MAX1 == "B":
                a1 = B1[0]
                b1 = C1[0]
                gamma1 = A1[1]
                colors1 = ["blue", "green", "red"]
                progression1 = ["B", "C", "A"]
            elif MAX1 == "C":
                a1 = C1[0]
                b1 = A1[0]
                gamma1 = B1[1]
                colors1 = ["green", "red", "blue"]
                progression1 = ["C", "A", "B"]
            if MAX2 == "A":
                a2 = A2[0]
                b2 = B2[0]
                gamma2 = C2[1]
                colors2 = ["red", "blue", "green"]
                progression2 = ["A", "B", "C"]
            elif MAX2 == "B":
                a2 = B2[0]
                b2 = C2[0]
                gamma2 = A2[1]
                colors2 = ["blue", "green", "red"]
                progression2 = ["B", "C", "A"]
            elif MAX2 == "C":
                a2 = C2[0]
                b2 = A2[0]
                gamma2 = B2[1]
                colors2 = ["green", "red", "blue"]
                progression2 = ["C", "A", "B"]
            #a1 = A1[0]
            #a2 = A2[0]
            #b1 = B1[0]
            #b2 = B2[0]
            #gamma1 = C1[1]
            #gamma2 = C2[1]
            progressions = [progression1,progression2]
            colors1 = ["black"]*3
            colors2 = ["black"]*3
            TEX = get_TEX2(a1, b1, gamma1, a2, b2, gamma2, colors1, colors2, RESULTS, TEX, progressions, sides)
        with open('doc1.tex', 'w') as out_file:
            for t in TEX:
                out_file.write("%s\n" % t)
        file = "doc1.pdf"
        if True:
            call(['pdflatex', 'doc1.tex'])
            if current_os == "Windows":
                os.startfile(file)
            elif current_os == "Linux":
                call(["xdg-open", file])
            else:  # macOS
                call(["open", file])
        pprint.pprint(RESULTS)
        #pprint.pprint(MAX)