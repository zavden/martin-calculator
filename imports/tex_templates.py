from math import pi


def ROUND(n):
    return round(round(n, 6), 2)


def get_TEX(a,b,gamma,colors,values,more_tex=[],progression=[],SIDES={},max_width=10):
    A,B,C = progression
    width = SIDES[A]["side"]
    ratio = max_width / width
    max_a = max_width
    max_b = SIDES[B]["side"] * ratio
    max_c = SIDES[C]["side"] * ratio
    _alpha = SIDES[A]["angle"]
    _beta = SIDES[B]["angle"]
    _gamma = SIDES[C]["angle"]
    r = ratio
    return (
        r"""\documentclass[12pt,a4paper]{article}
        \usepackage[a4paper,margin=1cm,landscape]{geometry}
        \usepackage{tabu}
        \pagestyle{empty}
        \usepackage{tikz}
        \usepackage{longtable}
        \usepackage{mwe}
        \usepackage{amsmath, nccmath}
        \usepackage{array}
        \usepackage[spanish]{babel}
        \newcolumntype{P}[1]{>{\centering\arraybackslash}m{#1}}
        \newcommand{\centerto}[2]{% 
          {\ooalign{$\displaystyle\hphantom{#2}$\cr 
             \hidewidth$\displaystyle#1$\hidewidth\cr}}} 
        \begin{document}
        \begin{longtable}[t]{P{5cm} p{18cm} l}""",
        # r"\draw(0,0)--(1,1);",
        # r"\begin{figure}",
        # r"\centering",
        # r"\resizebox{\columnwidth}{!}{%",
        *more_tex,
        #r"\begin{center}",
        #r"\end{center}%",
        # r"}",
        # r"\end{figure}",
        #r"\begin{align*}",
        #r"\end{align*}",
        r"\end{longtable}",
        #r"\begin{longtable}[t]{cc}",
        #r"\begin{tikzpicture}",
        #r"\draw[%s,very thick](0,0)--(%f,0);"%(colors[0],a),
        #r"\draw[%s,very thick](0,0)--({%f*cos(%f)},{%f*sin(%f)});"%(colors[1],b,gamma,b,gamma),
        #r"\draw[%s,very thick](%f,0)--({%f*cos(%f)},{%f*sin(%f)});"%(colors[2],a,b,gamma,b,gamma),
        #r"\end{tikzpicture} & ",
        #r"\scalebox{1.4}{$\begin{aligned}",
        #r"& {\color{red}a = %g}\\"%(values[0][0]),
        #r"& {\color{blue}b = %g}\\"%(values[1][0]),
        #r"& {\color{green}c = %g}\\"%(values[2][0]),
        #r"& {\color{red}\alpha = %g}\\"%(values[0][1]),
        #r"& {\color{blue}\beta = %g}\\" %(values[1][1]),
        #r"& {\color{green}\gamma = %g}"%(values[2][1]),
        #r"\end{aligned}$}",
        #r"\end{longtable}",
        # TEST
        r"\begin{center}",
        r"\scalebox{1.4}{\begin{tikzpicture}",
        r"\draw[%s,very thick](0,0) node [below left] {$%s=%g^\circ$}--({%f},0) node [midway,below] {$%s=%g$};" % (colors[0],get_symbol(C),_gamma, a*r,A.lower(),SIDES[A]["side"]),
        r"\draw[%s,very thick](0,0)--({%f*cos(%f)},{%f*sin(%f)}) node [midway,above left] {$%s=%g$} node [above] {$%s=%g^\circ$};" % (colors[1], b*r, gamma, b*r, gamma, B.lower(),SIDES[B]["side"],get_symbol(A), _alpha),
        r"\draw[%s,very thick]({%f},0)  node [below right] {$%s=%g^\circ$} --({%f*cos(%f)},{%f*sin(%f)}) node [midway,above right] {$%s=%g$};" % (colors[2], a*r, get_symbol(B), _beta,b*r, gamma, b*r, gamma, C.lower(), SIDES[C]["side"]),
        r"\end{tikzpicture}}",
        r"\end{center}",
        r"\end{document}",
    )


def get_TEX2(a1,b1,gamma1,a2,b2,gamma2,colors1, colors2, values,more_tex,progression=[],SIDES=[],max_width=10):
    A1, B1, C1 = progression[0]
    A2, B2, C2 = progression[1]
    width1 = SIDES[0][A1]["side"]
    width2 = SIDES[1][A2]["side"]
    ratio1 = max_width / width1
    ratio2 = max_width / width2
    max_a1 = max_width
    max_a2 = max_width
    max_b1 = SIDES[0][B1]["side"] * ratio1
    max_b2 = SIDES[1][B2]["side"] * ratio2
    max_c1 = SIDES[0][C1]["side"] * ratio1
    max_c2 = SIDES[1][C2]["side"] * ratio2
    _alpha1 = SIDES[0][A1]["angle"]
    _beta1 = SIDES[0][B1]["angle"]
    _gamma1 = SIDES[0][C1]["angle"]
    _alpha2 = SIDES[1][A2]["angle"]
    _beta2 = SIDES[1][B2]["angle"]
    _gamma2 = SIDES[1][C2]["angle"]
    r1 = ratio1
    r2 = ratio2
    return (
        r"""\documentclass[12pt,a4paper]{article}
        \usepackage[a4paper,margin=1cm,landscape]{geometry}
        \usepackage{tabu}
        \pagestyle{empty}
        \usepackage{tikz}
        \usepackage{longtable}
        \usepackage{mwe}
        \usepackage{amsmath, nccmath}
        \usepackage{array}
        \usepackage[spanish]{babel}
        \newcolumntype{P}[1]{>{\centering\arraybackslash}m{#1}}
        \newcommand{\centerto}[2]{% 
          {\ooalign{$\displaystyle\hphantom{#2}$\cr 
             \hidewidth$\displaystyle#1$\hidewidth\cr}}} 
        \begin{document}
        \begin{center} Option 1 \end{center}
        \begin{longtable}[t]{P{6cm} p{16cm} l}""",
        # r"\draw(0,0)--(1,1);",
        # r"\begin{figure}",
        # r"\centering",
        # r"\resizebox{\columnwidth}{!}{%",
        *more_tex[0],
        r"\end{longtable}",
        r"\begin{center}",
        r"\scalebox{1.4}{\begin{tikzpicture}",
        r"\draw[%s,very thick](0,0) node [below left] {$%s=%g^\circ$}--({%f},0) node [midway,below] {$%s=%g$};" %(
                colors2[0],                 get_symbol(C1),ROUND(gamma1),a1*r1,A1.lower(),a1),
        r"\draw[%s,very thick](0,0)--({%f*cos(%f)},{%f*sin(%f)}) node [midway,above left] {$%s=%g$} node [above] {$%s=%g^\circ$};" %(
                colors2[1],         b1*r1, gamma1, b1*r1, gamma1,                   B1.lower(),b1,    get_symbol(A1),ROUND(_alpha1)),
        r"\draw[%s,very thick]({%f},0)  node [below right] {$%s=%g^\circ$} --({%f*cos(%f)},{%f*sin(%f)}) node [midway,above right] {$%s=%g$};" % (
                colors2[2],    a1*r1,           get_symbol(B1),ROUND(_beta1),b1*r1, gamma1, b1*r1, gamma1, C1.lower(), SIDES[0][C1]["side"]),
        r"\end{tikzpicture}}",
        r"\end{center}",
        #r"\scalebox{1.4}{$\begin{aligned}",
        #r"& {\color{red}a = %g}\\" % (values[0][0][0]),
        #r"& {\color{blue}b = %g}\\" % (values[0][1][0]),
        #r"& {\color{green}c = %g}\\" % (values[0][2][0]),
        #r"& {\color{red}\alpha = %g}\\" % (values[0][0][1]),
        #r"& {\color{blue}\beta = %g}\\" % (values[0][1][1]),
        #r"& {\color{green}\gamma = %g}" % (values[0][2][1]),
        #r"\end{aligned}$}",
        #r"\end{longtable}",
        # r"}",
        # r"\end{figure}%",
        # r"\draw(0,0)--(1,1);",
        r"\begin{center} Option 2 \end{center}",
        r"\begin{longtable}[t]{P{6cm} p{16cm} l}",
        *more_tex[1],
        #
        # r"\begin{figure}",
        # r"\centering",
        # r"\resizebox{\columnwidth}{!}{%",
        r"\end{longtable}",
        # r"}",
        # r"\end{figure}",
        # r"\draw(0,0)--(1,1);",
        #
        r"\begin{center}",
        r"\scalebox{1.4}{\begin{tikzpicture}",
        r"\draw[%s,very thick](0,0) node [below left] {$%s=%g^\circ$}--({%f},0) node [midway,below] {$%s=%g$};" % (colors2[0],get_symbol(C2),_gamma2, a2*r2,A2.lower(),a2),
        r"\draw[%s,very thick](0,0)--({%f*cos(%f)},{%f*sin(%f)}) node [midway,above left] {$%s=%g$} node [above] {$%s=%g^\circ$};" % (colors2[1], b2*r2, gamma2, b2*r2, gamma2, B2.lower(),b2,get_symbol(A2), ROUND(_alpha2)),
        r"\draw[%s,very thick]({%f},0)  node [below right] {$%s=%g^\circ$} --({%f*cos(%f)},{%f*sin(%f)}) node [midway,above right] {$%s=%g$};" % (colors2[2], a2*r2, get_symbol(B2), ROUND(_beta2),b2*r2, gamma2, b2*r2, gamma2, C2.lower(), SIDES[1][C2]["side"]),
        r"\end{tikzpicture}}",
        r"\end{center}",
        r"\end{document}",
    )


# SSS TEX FUNCS
def get_side_lll(tex,l1,l2,l3,a1,f1,f2,f3):
    angle = get_symbol(f1.upper())
    t1 = r"& \phantom{cos^{-1}\Big(\ \,\,} \centerto{%s^2}{%g^2} \phantom{\centerto{%s^2}{%g^2} + \centerto{%s^2}{%g^2} =\Big)}= %s^2 + %s^2 - 2%s%s \cdot\cos%s\\"%(
                                                      f1,   l1,                      f2,   l2,                f3,   l3,           f2,   f3,    f2,f3,        angle)
    t2 = r"& \phantom{cos^{-1}\Big(\ \,\,} \centerto{%s^2}{%g^2} - \centerto{%s^2}{%g^2} + \centerto{%s^2}{%g^2} \phantom{\Big)} = \phantom{%s^2 + %s^2} - 2%s%s \cdot\cos%s\\"%(
                                                      f1,   l1,               f2,   l2,               f3,    l3,                            f2,    f3,      f2,f3,        angle)
    t3 = r"& \phantom{cos^{-1}\Big(\ \,} \frac{\centerto{%s^2}{%g^2} - \centerto{%s^2}{%g^2} + \centerto{%s^2}{%g^2}}{- 2%s%s} \phantom{\Big)} = \phantom{%s^2 + %s^2 - 2%s%s \cdot\,} \cos%s\\"%(
                                                          f1,   l1,               f2,   l2,               f3,   l3,     f2,f3,                             f2,   f3,    f2,f3,       angle)
    t4 = r"& \cos^{-1}\left(\frac{\centerto{%s^2}{%g^2} - \centerto{%s^2}{%g^2} + \centerto{%s^2}{%g^2}}{- 2%s%s}\right) = \phantom{%s^2 + %s^2 - 2%s%s \cdot\cos\!} %s\\"%(
                                             f1,    l1,              f2,   l2,               f3,    l3,    f2,f3,                   f2,    f3,    f2,f3,         angle)
    t5 = r"& \cos^{-1}\left(\frac{%g^2 - %g^2 + %g^2}{- 2\cdot %g\cdot %g}\right) = \phantom{%s^2 + %s^2 - 2%s%s \cdot\cos\!} %s\\"%(
                                   l1,    l2,    l3,           l2,     l3,                    f2,    f3,    f2,f3,           angle)
    t6 = r"& %g^\circ = %s"%(a1,angle)
    # te = r"$$%s = \cos^{-1}{\left(\frac{%g^2 + %g^2 - %g^2}{2\cdot %g \cdot %g}\right)} = %g^\circ$$"%(angle,l2,l3,l1,l2,l3,a1)
    tex.append(r"\scalebox{1.5}{$\begin{aligned}")
    #tex.append(r"& \mbox{Calculating $%s$}\\"%angle)
    tex.append(t1)
    tex.append(t2)
    tex.append(t3)
    tex.append(t4)
    tex.append(t5)
    #tex.append(t6)
    tex.append(r"\end{aligned}$} & ")
    # tex.append(te)

# def get_side_2(tex,l1,l2,l3,a1):
#     t1 = r"$$\beta = \arccos{\left(\frac{%g^2 + %g^2 - %g^2}{2\cdot %g \cdot %g}\right)} = %g^\circ$$"%(l2,l3,l1,l2,l3,a1)
#     tex.append(t1)


def get_side_3(tex,a1,a2,a3):
    t1 = r"& \alpha + \beta + \gamma = 180^\circ \\"
    t2 = r"& \phantom{\alpha + \beta +\ \,}\gamma = 180^\circ - \centerto{\alpha}{%g^\circ} - \centerto{\beta}{%g^\circ} \\"%(a1,a2)
    te = r"& \phantom{\alpha + \beta +\ \,}\gamma = 180^\circ - %g^\circ - %g^\circ"%(a1,a2)
    #tex.append(r"Calculating $\gamma$")
    tex.append(r"\scalebox{1.5}{$\begin{aligned}")
    tex.append(t1)
    tex.append(t2)
    tex.append(te)
    tex.append(r"\end{aligned}$} & ")


### SAS TEX FUNCS
###----------------------------
def get_side_1_lla(tex,s1,s2,a,l1,l2,a3,l3):
    a_symbol = get_symbol(a)
    t1 = r"& %s^2 = \ \ \ \centerto{%s^2}{%g^2} + \centerto{%s^2}{%g^2} - 2 \phantom{-}\centerto{%s}{%g}\phantom{-}\! \centerto{%s}{%g} \cdot\cos \centerto{%s}{%g}\\"%(
             a.lower(),        s1.lower(),l1,           s2.lower(),l2,         s1.lower(),l1,     s2.lower(),l2,      a_symbol, a3
    )
    t2 = r"& %s \ = \sqrt{\centerto{%s^2}{%g^2} + \centerto{%s^2}{%g^2} - 2 \phantom{-}\centerto{%s}{%g}\phantom{-}\! \centerto{%s}{%g} \cdot\cos \centerto{%s}{%g}}\\"%(
             a.lower(),        s1.lower(),l1,           s2.lower(),l2,                        s1.lower(),l1,                   s2.lower(),l2,   a_symbol, a3
    )
    t3 = r"& %s \ = \sqrt{%g^2 + %g^2 - 2\cdot %g\cdot %g \cdot \cos %g^\circ}\\"%(
        a.lower(),l1,l2,l1,l2,a3
    )
    t4 = r"& %s = %g"%(a.lower(),l3)
    t0 = r"&\mbox{Cosine law}\\"
    tex.append(r"\scalebox{1.5}{$\begin{aligned}")
    #tex.append(t0)
    tex.append(t1)
    tex.append(t2)
    tex.append(t3)
    #tex.append(r"\\\\\\")
    #tex.append(t4)
    tex.append(r"\end{aligned}$} & ")


def get_angle_lla(tex, a: str, s1: str, l3, l1, a3, a1):
    a_symbol = get_symbol(a)
    s1_symbol = get_symbol(s1)

    t1 = r"& \frac{\sin %s}{%s} = \phantom{sin^{-1}\Big(\,}\frac{\sin \centerto{%s}{(%g^\circ)}}{%s}\\"%(
                 s1_symbol,s1.lower(),                          a_symbol,a3,a.lower())
    t2 = r"& \sin %s = \phantom{sin^{-1}\Big(\,}\frac{\sin \centerto{%s}{(%g^\circ)}}{%s} \cdot \centerto{%s}{%g}\\"%(
                  s1_symbol,a_symbol, a3,             a.lower(),                       s1.lower(),l1)
    t3 = r"& \phantom{sin\,} %s = \sin^{-1}\left(\frac{\sin \centerto{%s}{(%g^\circ)}}{%s} \cdot \centerto{%s}{%g}\right)\\"%(
                             s1_symbol,a_symbol,a3,   a.lower(),s1.lower(),l1)
    t4 = r"& \phantom{sin\,} %s = \sin^{-1}\left(\frac{\sin (%g^\circ)}{%g} \cdot %g\right)\\"%(
                             s1_symbol,                      a3,        l3,       l1)
    t5 = r"& %s = %g"%(s1_symbol,a1)

    t0 = r"& \mbox{Sine Law}\\"
    tex.append(r"\scalebox{1.5}{$\begin{aligned}")
    #tex.append(t0)
    tex.append(t1)
    tex.append(t2)
    tex.append(t3)
    tex.append(t4)
    #tex.append(t5)
    tex.append(r"\end{aligned}$} & ")

def get_last_angle_lla(tex, a, s2, s1, a3, a2, a1):
    # s1 = get_symbol(a1)
    # s2 = get_symbol(a2)
    # s3 = get_symbol(a3)
    s1_s = get_symbol(s1)
    s2_s = get_symbol(s2)
    a_s = get_symbol(a)

    t0 = r"& \mbox{Sum angles}\\"
    t1 = r"& \alpha + \beta + \gamma = 180^\circ \\"
    
    if s2_s == r"\alpha":
        t2 = r"& %s = 180^\circ - \centerto{%s}{%g^\circ} - \centerto{%s}{%g^\circ} \\"%(s2_s, a_s, a3,s1_s,a1)
        t3 = r"& %s = 180^\circ - %g^\circ - %g^\circ \\"%(s2_s, a3, a1)
        t4 = r"& %s = %g^\circ \\"%(s2_s, a2)
    elif s2_s == r"\gamma":
        t2 = r"& \phantom{\alpha + --\ } %s = 180^\circ - \centerto{%s}{%g^\circ} - \centerto{%s}{%g^\circ}  \\"%(s2_s, s1_s,a1, a_s,a3)
        t3 = r"& \phantom{\alpha + --\ } %s = 180^\circ - %g^\circ - %g^\circ \\"%(s2_s, a1, a3)
        t4 = r"& %s = %g^\circ"%(s2_s, a2)
    elif s2_s == r"\beta":
        t2 = r"& \phantom{\alpha +\ }%s \phantom{\alpha +\ \,}= 180^\circ - \centerto{%s}{%g^\circ} - \centerto{%s}{%g^\circ} \\"%(s2_s, s1_s,a1,a_s,a3)
        t3 = r"& \phantom{\alpha +\ }%s \phantom{\alpha +\ \,}= 180^\circ - %g^\circ - %g^\circ \\"%(s2_s, a1, a3)
        t4 = r"& %s = %g^\circ"%(s2_s, a2)
    tex.append(r"\scalebox{1.5}{$\begin{aligned}")
    #tex.append(t0)
    tex.append(t1)
    tex.append(t2)
    tex.append(t3)
    tex.append(r"\\\\")
    # tex.append(r"\vfill")
    #tex.append(t4)
    tex.append(r"\end{aligned}$} \vfill & ")


def get_last_angle_ala(tex, a, s2, s1, a3, a2, a1):
    s1_s = get_symbol(s1)
    s2_s = get_symbol(s2)
    a_s = get_symbol(a)

    t0 = "Sum angles"
    t1 = r"& \alpha + \beta + \gamma = 180^\circ \\"

    if a_s == r"\alpha":
        t2 = r"& %s \phantom{+\beta+\gamma\ \,}= 180^\circ - %s - %s \\" % (a_s, s1_s, s2_s)
        t3 = r"& %s \phantom{+\beta+\gamma\ \,}= 180^\circ - %g^\circ - %g^\circ \\" % (a_s, a1, a2)
        t4 = r"& %s \phantom{+\beta+\gamma}= %g^\circ" % (a_s, a3)
    elif a_s == r"\beta":
        t2 = r"& \phantom{\alpha +\ } %s \phantom{\alpha +\ \,}= 180^\circ - %s - %s \\" % (a_s, s1_s, s2_s)
        t3 = r"& \phantom{\alpha +\ } %s \phantom{\alpha +\ \,}= 180^\circ - %g^\circ - %g^\circ \\" % (a_s, a1, a2)
        t4 = r"& %s = %g^\circ" % (a_s, a3)
    elif a_s == r"\gamma":
        t2 = r"& \phantom{\alpha + --\ } %s = 180^\circ - %s - %s \\" % (a_s, s1_s, s2_s)
        t3 = r"& \phantom{\alpha + --\ } %s = 180^\circ - %g^\circ - %g^\circ \\" % (a_s, a1, a2)
        t4 = r"& %s = %g^\circ" % (a_s, a3)
    # if a == "A":
    # t2 = r"& %s = 180^\circ - %s - %s \\"%(a_s, s1_s, s2_s)
    #t3 = r"& %s = 180^\circ - %g^\circ - %g^\circ \\"%(a_s, a1, a2)
    # t4 = r"& %s = %g^\circ"%(a_s, a3)
    tex.append(r"\scalebox{1.5}{$\begin{aligned}")
    #tex.append(t0)
    tex.append(t1)
    tex.append(t2)
    tex.append(t3)
    #tex.append(t4)
    tex.append(r"\end{aligned}$} \vfill & ")


def get_last_angle_aal(tex, faltante, s2, s1, a3, a2, a1):
    s1_s = get_symbol(s1)
    s2_s = get_symbol(s2)
    a_s = get_symbol(faltante)

    t0 = "Sum angles"
    t1 = r"& \alpha + \beta + \gamma = 180^\circ \\"

    values = [
        [s1, s1_s, a1],
        [s2, s2_s, a2],
    ]

    # if faltante == "A":
    #     sort_orders = sorted(values, key=lambda x: x[0], reverse=True)
    # else:
    sort_orders = sorted(values, key=lambda x: x[0])

    v1,v2 = sort_orders
    s1, s1_s, a1 = v1
    s2, s2_s, a2 = v2
    
    # if a == "A":
    if a_s == r"\alpha":
        t2 = r"& %s \phantom{+\beta+\gamma\ \,}= 180^\circ - \centerto{%s}{%g^\circ} - \centerto{%s}{%g^\circ} \\" % (a_s, s1_s,a1,s2_s,a2)
        t3 = r"& %s \phantom{+\beta+\gamma\ \,}= 180^\circ - %g^\circ - %g^\circ \\" % (a_s, a1, a2)
        t4 = r"& %s \phantom{+\beta+\gamma}= %g^\circ" % (a_s, a3)
    elif a_s == r"\beta":
        t2 = r"& \phantom{\alpha +\ } %s \phantom{\alpha +\ \,}= 180^\circ - \centerto{%s}{%g^\circ} - \centerto{%s}{%g^\circ} \\" % (a_s, s1_s,a1,s2_s,a2)
        t3 = r"& \phantom{\alpha +\ } %s \phantom{\alpha +\ \,}= 180^\circ - %g^\circ - %g^\circ \\" % (a_s, a1, a2)
        t4 = r"& %s = %g^\circ" % (a_s, a3)
    elif a_s == r"\gamma":
        t2 = r"& \phantom{\alpha + --\ } %s = 180^\circ - \centerto{%s}{%g^\circ} - \centerto{%s}{%g^\circ} \\" % (a_s, s1_s,a1,s2_s,a2)
        t3 = r"& \phantom{\alpha + --\ } %s = 180^\circ - %g^\circ - %g^\circ \\" % (a_s, a1, a2)
        t4 = r"& %s = %g^\circ" % (a_s, a3)
    # tex.append(t0)
    tex.append(r"\scalebox{1.5}{$\begin{aligned}")
    tex.append(t1)
    tex.append(t2)
    tex.append(t3)
    # tex.append(t4)
    tex.append(r"\end{aligned}$} \vfill & ")


def get_side_from_ala(tex, a, b, a_n, alpha, b_n, beta):
    a_s = get_symbol(a)
    b_s = get_symbol(b)
    t0 = "Sine law"
    t1 = r"& \frac{%s}{\sin%s}=\frac{%s}{\sin\centerto{%s}{(%g^\circ)}} \\"%(
                a.lower(),a_s,   b.lower(),b_s,beta
    )
    t2 = r"& \phantom{-\,}%s\phantom{-}=\frac{%s}{\sin\centerto{%s}{(%g^\circ)}}\cdot \sin\centerto{%s}{%g^\circ)} \\"%(
                a.lower(),  b.lower(), b_s,beta,a_s,alpha
    )
    t4 = r"& \phantom{-\,}%s\phantom{-}=\frac{%g}{\sin(%g^\circ)}\cdot \sin (%g^\circ) \\"%(
            a.lower(),  b_n, beta, alpha 
    )
    t5 = r"& %s=%g"%(
            a.lower(),  a_n
    )
    # tex.append(t0)
    tex.append(r"\scalebox{1.5}{$\begin{aligned}")
    tex.append(t1)
    tex.append(t2)
    tex.append(t4)
    # tex.append(t5)
    tex.append(r"\end{aligned}$} \vfill & ")


def get_side_from_aal(tex, b, beta, b_n, a, alpha, a_n):
    a_s = get_symbol(a)
    b_s = get_symbol(b)
    t0 = "Sine law"
    t1 = r"& \frac{%s}{\sin%s}=\frac{%s}{\sin%s} \\"%(
                a.lower(),a_s,   b.lower(),b_s,
    )
    t2 = r"& \phantom{-\,}%s\phantom{-}=\frac{%s}{\sin%s}\cdot \sin%s \\"%(
                a.lower(),  b.lower(), b_s, a_s 
    )
    t4 = r"& \phantom{-\,}%s\phantom{-}=\frac{%g}{\sin(%g^\circ)}\cdot \sin (%g^\circ) \\"%(
            a.lower(),  b_n, beta, alpha 
    )
    t5 = r"& %s=%g "%(
            a.lower(),  a_n
    )

    #tex.append(t0)
    tex.append(r"\scalebox{1.5}{$\begin{aligned}")
    tex.append(t1)
    tex.append(t2)
    tex.append(t4)
    # tex.append(t5)
    tex.append(r"\end{aligned}$} \vfill & ")


def get_angle_from_sin_lla(tex, os, swa, c, b, a3, y):
    os_s = get_symbol(os)
    swa_s = get_symbol(swa)
    db = ROUND(a3*180/pi)
    t1 = r"& \frac{\sin%s}{%s} = \phantom{sin^{-1}\Big(\,} \frac{\sin\centerto{%s}{(%g^\circ)}}{%s} \\"%(
                      os_s,os.lower(),                            swa_s,db,swa.lower()
    )
    t2 = r"& \sin%s = \phantom{sin^{-1}\Big(\,} \frac{\sin\centerto{%s}{(%g^\circ)}}{%s} \cdot \centerto{%s}{%g} \\"%(
                 os_s,                                 swa_s,db,swa.lower(),os.lower(),c
    )
    t3 = r"& \phantom{sin\,} %s = \sin^{-1}\left(\frac{\sin\centerto{%s}{(%g^\circ)}}{%s} \cdot \centerto{%s}{%g}\right) \\"%(
                             os_s,                      swa_s,db,swa.lower(),os.lower(),c
    )
    t4 = r"& \phantom{sin\,} %s = \sin^{-1}\left(\frac{\sin(%g^\circ)}{%g} \cdot %g \right) \\"%(
                             os_s,             ROUND(a3*180/pi),        b,        c
    )
    t5 = r"& %s = %g^\circ"%(os_s, ROUND(y*180/pi))
    #tex.append(r"Sine law")
    tex.append(r"\scalebox{1.5}{$\begin{aligned}")
    tex.append(t1)
    tex.append(t2)
    tex.append(t3)
    tex.append(t4)
    # tex.append(t5)
    tex.append(r"\end{aligned}$} & ")


def show_optusangle(tex, os, y1, y2):
    a_s = get_symbol(os)

    #if a_s == r"\alpha":
    #    t1 = r"& %s' \phantom{+\beta'+\gamma'\ \,}= 180^\circ - %s \\" % (get_symbol(os), get_symbol(os))
    #    t2 = r"& %s' \phantom{+\beta'+\gamma'\ \,}= 180^\circ - %g \\" % (get_symbol(os), y1)
    #    t3 = r"& %s' \phantom{+\beta+\gamma\ \,}= %g^\circ " % (get_symbol(os), y2)
    #elif a_s == r"\beta":
    #    t1 = r"& \phantom{\alpha +\ } %s' \phantom{\alpha +\ \,} = 180^\circ - %s \\" % (get_symbol(os), get_symbol(os))
    #    t2 = r"& \phantom{\alpha +\ } %s' \phantom{\alpha +\ \,} = 180^\circ - %g \\" % (get_symbol(os), y1)
    #    t3 = r"& \phantom{\alpha +\ } %s' \phantom{\alpha +\ \,} = %g^\circ " % (get_symbol(os), y2)
    #elif a_s == r"\gamma":
    #    t1 = r"& \phantom{\alpha + --\ } %s' = 180^\circ - %s \\" % (get_symbol(os), get_symbol(os))
    #    t2 = r"& \phantom{\alpha + --\ } %s' = 180^\circ - %g \\" % (get_symbol(os), y1)
    #    t3 = r"& \phantom{\alpha + --\ } %s' = %g^\circ " % (get_symbol(os), y2)

    t1 = r"& %s' = 180^\circ - \centerto{%s}{%g^\circ} \\" % (get_symbol(os), get_symbol(os),y1)
    t2 = r"& %s' = 180^\circ - %g^\circ \\" % (get_symbol(os), y1)
    # t3 = r"& %s' = %g^\circ " % (get_symbol(os), y2)

    tex.append(r"\scalebox{1.5}{$\begin{aligned}")
    tex.append(t1)
    tex.append(t2)
    #tex.append(t3)
    tex.append(r"\end{aligned}$} & ")


def get_last_angle_aal_prime(tex, faltante, s2, s1, a3, a2, a1):
    s1_s = get_symbol(s1)
    s2_s = get_symbol_prime(s2)
    a_s = get_symbol(faltante)

    t0 = "Sum angles"

    values = [
        [s1 ,s1_s, a1],
        [s2 ,s2_s, a2],
    ]
    values2 = [
        [s1 ,s1_s, a1],
        [s2 ,s2_s, a2],
        [faltante ,a_s, a3],
    ]
    sort_orders2 = sorted(values2, key=lambda x: x[0])
    m1, m2, m3 = sort_orders2
    sort_orders = sorted(values, key=lambda x: x[0])
    t1 = r"& %s + %s + %s= 180^\circ \\"%(m1[1],m2[1],m3[1])

    v1,v2 = sort_orders
    s1, s1_s, a1 = v1
    s2, s2_s, a2 = v2
    
    # if a == "A":
    #t2 = r"& %s = 180^\circ - %s - %s \\"%(a_s, s1_s, s2_s)
    #t3 = r"& %s = 180^\circ - %g^\circ - %g^\circ \\"%(a_s, a1, a2)
    #t4 = r"& %s = %g^\circ "%(a_s, a3)

    if a_s == r"\alpha":
        t2 = r"& %s \phantom{+\beta+\gamma\ \,\,}= 180^\circ - \centerto{%s}{%g^\circ} - \centerto{%s}{%g^\circ} \\" % (a_s, s1_s,a1,s2_s,a2)
        t3 = r"& %s \phantom{+\beta+\gamma\ \,\,}= 180^\circ - %g^\circ - %g^\circ \\" % (a_s, a1, a2)
        t4 = r"& %s \phantom{+\beta+\gamma}= %g^\circ" % (a_s, a3)
    elif a_s == r"\beta":
        if s1_s == r"\gamma":
            t2 = r"& \phantom{\alpha +\ \,} %s \phantom{\alpha +\ \,\,}= 180^\circ - \centerto{%s}{%g^\circ} - \centerto{%s}{%g^\circ} \\" % (a_s, s1_s,a1,s2_s,a2)
            t3 = r"& \phantom{\alpha +\ \,} %s \phantom{\alpha +\ \,\,}= 180^\circ - %g^\circ - %g^\circ \\" % (a_s, a1, a2)
            t4 = r"& %s = %g^\circ" % (a_s, a3)
        else:
            t2 = r"& \phantom{\alpha +\ \,} %s \phantom{\alpha +\ \,}= 180^\circ - \centerto{%s}{%g^\circ} - \centerto{%s}{%g^\circ} \\" % (a_s, s1_s,a1,s2_s,a2)
            t3 = r"& \phantom{\alpha +\ \,} %s \phantom{\alpha +\ \,}= 180^\circ - %g^\circ - %g^\circ \\" % (a_s, a1, a2)
            t4 = r"& %s = %g^\circ" % (a_s, a3)
    elif a_s == r"\gamma":
        t2 = r"& \phantom{\alpha + --\ \,} %s = 180^\circ - \centerto{%s}{%g^\circ} - \centerto{%s}{%g^\circ} \\" % (a_s, s1_s,a1,s2_s,a2)
        t3 = r"& \phantom{\alpha + --\ \,} %s = 180^\circ - %g^\circ - %g^\circ \\" % (a_s, a1, a2)
        t4 = r"& %s = %g^\circ" % (a_s, a3)
    # tex.append(t0)
    tex.append(r"\scalebox{1.5}{$\begin{aligned}")
    tex.append(t1)
    tex.append(t2)
    tex.append(t3)
    #tex.append(t4)
    tex.append(r"\end{aligned}$} & ")


def get_symbol(symbol):
    if symbol == "A":
        return r"\alpha"
    elif symbol == "B":
        return r"\beta"
    elif symbol == "C":
        return r"\gamma"
    else:
        return "ERROR EN get_symbol"


def get_symbol_prime(symbol):
    if symbol == "A":
        return r"\alpha'"
    elif symbol == "B":
        return r"\beta'"
    elif symbol == "C":
        return r"\gamma'"
    else:
        return "ERROR EN get_symbol_prime"


def get_tikz_figure():
    return r"""
    \begin{tikzpicture}
    \draw[red,very thick](0,0)--({2.000000*2},0);
    \draw[blue,very thick](0,0)--({3.000000*cos(65.381532)*2*0.4},{3.000000*sin(65.381532)*2});
    \draw[green,very thick]({2.000000*2},0)--({3.000000*cos(65.381532)*2*0.4},{3.000000*sin(65.381532)*2});
    \end{tikzpicture} &
    """


def get_tikz_figure_sss_1(l1,l2,l3):
    return r"""
        \begin{tikzpicture}
        \draw[red,very thick](0,0)--({2.000000*2},0) node [midway,below] {$%g$};
        \draw[blue,very thick](0,0)--({3.000000*cos(65.381532)*2*0.4},{3.000000*sin(65.381532)*2}) node [midway,above left] {$%g$};
        \draw[green,very thick]({2.000000*2},0)--({3.000000*cos(65.381532)*2*0.4},{3.000000*sin(65.381532)*2}) node [midway, above right] {$%g$};
        \end{tikzpicture} &
        """ % (l1, l2, l3)


def get_tikz_figure_sss_2(l1,l2,l3,a1):
    return r"""
        \begin{tikzpicture}
        \draw[red,very thick](0,0) node [above right] {$%g^\circ$}--({2.000000*2},0) node [midway,below] {$%g$};
        \draw[blue,very thick](0,0)--({3.000000*cos(65.381532)*2*0.4},{3.000000*sin(65.381532)*2}) node [midway,above left] {$%g$} ;
        \draw[green,very thick]({2.000000*2},0) --({3.000000*cos(65.381532)*2*0.4},{3.000000*sin(65.381532)*2}) node [midway, above right] {$%g$};
        \end{tikzpicture} &
        """%(a1,l1,l2,l3)


def get_tikz_figure_sss_3(l1,l2,l3,a1,a2):
    return r"""
        \begin{tikzpicture}
        \draw[red,very thick](0,0) node [above right] {$%g^\circ$}--({2.000000*2},0) node [midway,below] {$%g$};
        \draw[blue,very thick](0,0)--({3.000000*cos(65.381532)*2*0.4},{3.000000*sin(65.381532)*2}) node [midway,above left] {$%g$}  node [below,yshift=-1.6cm,xshift=3mm] {$%g^\circ$};
        \draw[green,very thick]({2.000000*2},0) --({3.000000*cos(65.381532)*2*0.4},{3.000000*sin(65.381532)*2}) node [midway, above right] {$%g$};
        \end{tikzpicture} &
        """%(a1,l1,l2,a2,l3)


def get_tikz_figure_precomplete(SIDES):
    def tg(l,t):
        if SIDES[l][t] is False:
            return ""
        if t == "angle":
            return r"%g^\circ"%SIDES[l][t]
        elif t == "side":
            return r"%g"%SIDES[l][t]
    return_ = r"""
        \begin{tikzpicture}
        \draw[very thick](0,0) node [above right] {$%s$}--({2.000000*2},0) node [midway,below] {$%s$};
        \draw[very thick](0,0)--({3.000000*cos(65.381532)*2*0.4},{3.000000*sin(65.381532)*2}) node [midway,above left] {$%s$}  node [below,yshift=-1.6cm,xshift=3mm] {$%s$};
        \draw[very thick]({2.000000*2},0) node [above left, xshift=-2mm] {$%s$}--({3.000000*cos(65.381532)*2*0.4},{3.000000*sin(65.381532)*2}) node [midway, above right] {$%s$};
        \end{tikzpicture} &
        """%(
                    tg("C","angle"),tg("A","side"),
                    tg("B","side"),tg("A","angle"),
                    tg("B","angle"),tg("C","side"),
        )
    print(return_)
    return return_
