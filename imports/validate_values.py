
def check_values_enough(right_values,ERRORS):
    if len(right_values) < 3:
        ERRORS.append("There are not enough values")
        validate_numbers(right_values, ERRORS)
        print(ERRORS)
        # exit()
    elif len(right_values) > 3:
        ERRORS.append("There is too much data")
        validate_numbers(right_values, ERRORS)
        print(ERRORS)
        # exit()

def validate_numbers(l,ERRORS,close=False):
    inner = False
    negative = False
    for i in l:
        if i != "":
            try:
                float(i)
                if float(i) < 0:
                    if not negative:
                        ERRORS.append(f"There can be no negative numbers")
                        negative = True
            except ValueError:
                ERRORS.append(f"{i} is not a number")
                inner = True
                # if close:
                #     print(ERRORS)
                #     exit()
    if inner or negative:
        print(ERRORS)
        # exit()

