# import pprint
# from subprocess import call
# import os
from tkinter import *
# import platform
from math import acos,pi

from imports.func_solutions import (
    sol_lll,
    sol_lla,
    sol_aal,
    sol_ala,
    sol_lal,
)
#from imports.tex_templates import *
from imports.validate_values import *

# from tkinter import messagebox

import pprint
from math import pi, acos, sqrt, cos, sin, asin
from imports.run_pdflatex import run_latex

def print_tex(
        a: Entry,
        b: Entry,
        c: Entry,
        alpha: Entry,
        beta: Entry,
        gamma: Entry
    ):
    # current_os = platform.system()

    main_dict = {
        "a": a.get(),
        "b": b.get(),
        "c": c.get(),
        "alpha": alpha.get(),
        "beta": beta.get(),
        "gamma": gamma.get(),
    }
    main_dict_values = list(main_dict.values())
    right_values = list(filter(lambda x: x != "", main_dict_values))
    ERRORS = []
    TEX = []
    check_values_enough(right_values, ERRORS)
    validate_numbers(right_values, ERRORS, True)
    RESULTS = []

    if len(ERRORS) != 0:
        set_err = set(ERRORS)
        list_err = list(set_err)
        # messagebox.sh
        err = Error(list_err)
        err.run()

    SIDES = get_sides(main_dict)
    nL = len(list(filter(lambda x: x != "", [main_dict["a"], main_dict["b"], main_dict["c"]])))
    if len(ERRORS) == 0:
        if nL == 3:
            print("---> LLL")
            RESULTS = sol_lll(SIDES,ERRORS,Error,TEX)
        elif nL == 2:
            print("---> LAL or LLA")
            true_sides = list(filter(lambda x: SIDES[x]["side"] is not False, ["A", "B", "C"]))
            false_sides = list(filter(lambda x: SIDES[x]["side"] is False, ["A", "B", "C"]))
            order_sides = [*true_sides, *false_sides]
            print(order_sides)
            if SIDES[order_sides[-1]]["angle"] is not False:
                print("------> LAL")
                RESULTS = sol_lal(*order_sides, SIDES, ERRORS, Error, TEX)
            else:
                print("------> LLA")
                side_with_angle = list(filter(lambda x: SIDES[x]["angle"], true_sides))
                only_side = list(
                    filter(
                        lambda x: SIDES[x]["angle"] is False and SIDES[x]["side"] is not False,
                        true_sides
                    )
                )
                RESULTS = sol_lla(only_side[0], side_with_angle[0], SIDES, ERRORS, Error, TEX)
        elif nL == 1:
            print("---> ALA or AAL")
            true_angles = list(filter(lambda x: SIDES[x]["angle"] is not False, ["A", "B", "C"]))
            false_angles = list(filter(lambda x: SIDES[x]["angle"] is False, ["A", "B", "C"]))
            order_angles = [*true_angles, *false_angles]
            # print(order_angles)
            if SIDES[order_angles[-1]]["side"] is not False:
                print("------> ALA")
                print(order_angles)
                RESULTS = sol_ala(*order_angles, SIDES, TEX)
            else:
                print("------> AAL")
                side_with_angle = list(filter(lambda x: SIDES[x]["side"] is not False, true_angles))
                only_angle = list(
                    filter(
                        lambda x: SIDES[x]["side"] is False and SIDES[x]["angle"] is not False,
                        true_angles
                    )
                )
                # print(order_angles)
                args = [only_angle[0], side_with_angle[0], side_with_angle[0]]
                print(args)
                RESULTS = sol_aal(*args, SIDES, TEX)
        else:
            ERRORS.append("AAA has no solution")
            err_app = Error(ERRORS)
            err_app.run()
            # exit()

    run_latex(ERRORS, RESULTS, TEX, SIDES)



# def get_arc_cos(a,b,c):
#     return acos((b**2+c**2-a**2)/(2*b*c))*360/pi


def close(app: Tk):
    app.destroy()


class MainApp:
    def __init__(self):
        self.setup()
        self.construct()

    def run(self):
        self.mr.attributes('-type', 'dialog')
        self.mr.mainloop()

    def construct(self):
        mf = self.mf
        # label 1
        sl_1 = self.get_label(mf, text="a")
        sl_1.grid(row=0, column=0)
        se_1 = Entry(mf)
        se_1.grid(row=0, column=1)
        # ---------------
        # label 2
        sl_2 = self.get_label(mf, text="b")
        sl_2.grid(row=1,column=0)
        se_2 = Entry(mf)
        se_2.grid(row=1,column=1)
        # ---------------
        # label 3
        sl_3 = self.get_label(mf, text="c")
        sl_3.grid(row=2,column=0)
        se_3 = Entry(mf)
        se_3.grid(row=2,column=1)
        # ---------------
        # angle 1
        sa_1 = self.get_label(mf, text="alpha")
        sa_1.grid(row=3,column=0)
        sa_e1 = Entry(mf)
        sa_e1.grid(row=3,column=1)
        # ---------------
        # angle 1
        sa_2 = self.get_label(mf, text="beta")
        sa_2.grid(row=4,column=0)
        sa_e2 = Entry(mf)
        sa_e2.grid(row=4,column=1)
        # ---------------
        # angle 1
        sa_3 = self.get_label(mf, text="gamma")
        sa_3.grid(row=5, column=0)
        sa_e3 = Entry(mf)
        sa_e3.grid(row=5, column=1)
        # ---------------
        button = Button(
            mf, text="RUN",
            command=lambda: print_tex(se_1, se_2, se_3, sa_e1, sa_e2, sa_e3)
        )
        button.grid(row=6, column=1)

    def setup(self, *args, **kwargs):
        main_root = Tk()
        main_frame = Frame(main_root)
        # main_frame.config(width=400,height=400)
        main_frame.pack(*args, **kwargs)
        self.mr = main_root
        self.mf = main_frame

    def get_button(self, *args, **kwargs):
        return Button(*args,**kwargs)

    def get_label(self, *args, **kwargs):
        return Label(*args, **kwargs)


class Error(MainApp):
    def __init__(self, errors):
        super().setup()
        self.errors = errors
        self.construct()

    def construct(self):
        mf = self.mf
        # label 1
        for i,err in enumerate(self.errors):
            mensaje = self.get_label(mf, text=f"{err}")
            mensaje.grid(row=i, column=0)
        # ---------------
        button = Button(mf, text="Ok", command=lambda: close(self.mr))
        button.grid(row=len(self.errors)+1, column=0)


def get_sides(main_dict):
    SIDES = {
        "A": {
            "side": float(main_dict["a"]) if main_dict["a"] != "" else False,
            "angle": float(main_dict["alpha"]) if main_dict["alpha"] != "" else False
        },
        "B": {
            "side": float(main_dict["b"]) if main_dict["b"] != "" else False,
            "angle": float(main_dict["beta"]) if main_dict["beta"] != "" else False
        },
        "C": {
            "side": float(main_dict["c"]) if main_dict["c"] != "" else False,
            "angle": float(main_dict["gamma"]) if main_dict["gamma"] != "" else False
        },
    }
    return SIDES


if __name__ == "__main__":
    app = MainApp()
    app.run()