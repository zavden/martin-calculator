import pprint
from imports.func_solutions import *
from imports.main_dict import main_dict
import platform
from subprocess import call
import os
from imports.tex_templates import *
from imports.validate_values import *
from main import Error

main_dict_values = list(main_dict.values())

right_values = list(filter(lambda x: x != "", main_dict_values))

print(right_values)

# -----------------------------------------
#  _____ ____  ____   ___  ____  ____
# | ____|  _ \|  _ \ / _ \|  _ \/ ___|
# |  _| | |_) | |_) | | | | |_) \___ \
# | |___|  _ <|  _ <| |_| |  _ < ___) |
# |_____|_| \_\_| \_\\___/|_| \_\____/
# ------------------------------------------
ERRORS = []
TEX = []

check_values_enough(right_values,ERRORS)
# Verificar todos los datos
validate_numbers(right_values,ERRORS,True)

# ------------------------------
#      _     _
#  ___(_) __| | ___  ___
# / __| |/ _` |/ _ \/ __|
# \__ \ | (_| |  __/\__ \
# |___/_|\__,_|\___||___/
# ------------------------------


SIDES = {
    "A": {
        "side": float(main_dict["a"]) if main_dict["a"] != "" else False,
        "angle": float(main_dict["alpha"]) if main_dict["alpha"] != "" else False
    },
    "B": {
        "side": float(main_dict["b"]) if main_dict["b"] != "" else False,
        "angle": float(main_dict["beta"]) if main_dict["beta"] != "" else False
    },
    "C": {
        "side": float(main_dict["c"]) if main_dict["c"] != "" else False,
        "angle": float(main_dict["gamma"]) if main_dict["gamma"] != "" else False
    },
}

pprint.pprint(SIDES)

# SIDES_LIST = list(SIDES.items())

# pprint.pprint(SIDES_LIST)

nL = len(list(filter(lambda x: x != "",[main_dict["a"],main_dict["b"],main_dict["c"]])))

if nL == 3:
    print("---> LLL")
    RESULTS = sol_lll(SIDES)
elif nL == 2:
    print("---> LAL or LLA")
    true_sides = list(filter(lambda x: SIDES[x]["side"] != False,["A","B","C"]))
    false_sides = list(filter(lambda x: SIDES[x]["side"] == False,["A","B","C"]))
    order_sides = [*true_sides,*false_sides]
    print(order_sides)
    if SIDES[order_sides[-1]]["angle"] != False:
        print("------> LAL")
        RESULTS = sol_lal(*order_sides, SIDES)
    else:
        print("------> LLA")
        side_with_angle = list(filter(lambda x: SIDES[x]["angle"] != False,true_sides))
        only_side = list(filter(lambda x: SIDES[x]["angle"] == False and SIDES[x]["side"] != False,true_sides))
        RESULTS = sol_lla(only_side[0],side_with_angle[0], SIDES, ERRORS, Error, TEX)
elif nL == 1:
    print("---> ALA or AAL")
    true_angles = list(filter(lambda x: SIDES[x]["angle"] != False,["A","B","C"]))
    false_angles = list(filter(lambda x: SIDES[x]["angle"] == False,["A","B","C"]))
    order_angles = [*true_angles,*false_angles]
    # print(order_angles)
    if SIDES[order_angles[-1]]["side"] != False:
        print("------> ALA")
        print(order_angles)
        RESULTS = sol_ala(*order_angles, SIDES, TEX)
    else:
        print("------> AAL")
        side_with_angle = list(filter(lambda x: SIDES[x]["side"] != False,true_angles))
        only_angle = list(filter(lambda x: SIDES[x]["side"] == False and SIDES[x]["angle"] != False,true_angles))
        # print(order_angles)
        args = [only_angle[0],side_with_angle[0],side_with_angle[0]]
        print(args)
        RESULTS = sol_aal(*args, SIDES)
else:
    ERRORS.append("AAA no tiene solucion")
    print(ERRORS)
    exit()



if True:
    current_os = platform.system()
    if len(RESULTS) != 2:
        A,B,C = RESULTS
        a = A[0]
        b = B[0]
        gamma = C[1]
        print(f"a = {a}\t b = {b}\t gamma = {gamma}")
        TEX = get_TEX(a,b,gamma,RESULTS,TEX)
    else:
        A1,B1,C1 = RESULTS[0]
        A2,B2,C2 = RESULTS[1]
        a1 = A1[0]
        a2 = A2[0]
        b1 = B1[0]
        b2 = B2[0]
        gamma1 = C1[1]
        gamma2 = C2[1]
        TEX = get_TEX2(a1,b1,gamma1,a2,b2,gamma2,RESULTS,TEX)
    with open('doc1.tex','w') as out_file:
        for t in TEX:
            out_file.write("%s\n" % t)
    file = "doc1.pdf"
    call(['pdflatex','doc1.tex'])
    if current_os == "Windows":
        os.startfile(file)
    elif current_os == "Linux":
        call(["xdg-open", file])
    else: #macOS
        call(["open",file])


print("😃😃😃😃😃😃😃")